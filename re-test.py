import re

# операция или
line = 'test3'
result = re.search('test1|test2',line)
print(result)

# обработка группировкой
line = 'id           name                 password'

columns  = [ ]
result = re.search('^(\S+)\s+(\S+)\s+(\S+)$',line)

for i in range(1,4):
    columns.append(result.group(i))

print(columns)

# таблицы операторов регулярных выражений
# https://oval.mitre.org/language/about/re_support_5.6.html