import argparse

# в эту функцию парсер передаст управление при опции c
def clear( args ):
    print('Clear function')
    print(args)

# в эту функцию парсер передаст управление при опции b
def build ( args ):
    print('Build function')
    print(args)

# опция d
def decompose ( args ):
    print('Decompose function')
    print(vars(args))
    print(vars(args)['file'])

parser = argparse.ArgumentParser(description='Test argparse with subparsers')
subparsers = parser.add_subparsers(help='Options')

# добавление отдельных суб-парсеров
parser_decompose = subparsers.add_parser('d', help='Decompose main option')
# опцию file можно использовать только после указания d
parser_decompose.add_argument('file', type=str, help='Path to file to decompose')
# функция для вызова
parser_decompose.set_defaults(func=decompose)

parser_build = subparsers.add_parser('b', help='Build main option')
# функция для вызова
parser_build.set_defaults(func=build)


parser_clear = subparsers.add_parser('c', help='Clear main option')
# опцию --d можно использовать только после указания c
parser_clear.add_argument('--d', action='store_true', help='Remove .decomposed folder')
# функция для вызова
parser_clear.set_defaults(func=clear)

# парс и вызов функции, которая понадобится
args = parser.parse_args()
args.func(args)

