"""
Лямбда в формате lambda <вход> : <одна строчка выражения,

результат которого будет возвращён>. Вызов функции multi

возвращает lambda, что означает, по сути, возвращение

новой функции вида:

def doubler(a):
    return a*2

"""

def multi(n):
    return lambda a : a*n


doubler = multi(2)
r = doubler(90)
print(r)