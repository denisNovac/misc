
"""
Встроенный паттерн ДЕКОРАТОР принимает на вход 

функцию и пишет её имя и аргументы. Удобно для дебага

в случаях, когда не хочется делать лишние print.

func - аргумент, передаваемый функции display_arguments.

В такой аргумент можно передавать функции.

"""

def display_arguments(func):
    def display_and_call(*args, **kwargs):
        # сначала пишутся аргументы функции (и имя)
        print(f"- - - {getattr(func,'__name__')} {args} {kwargs}")
        # затем вызывается оригинальная функция
        return func(*args, **kwargs)
    return display_and_call

@display_arguments
def i_am_function(a,b):
    return a+b

print(i_am_function(5,7))