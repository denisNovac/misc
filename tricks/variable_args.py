
"""
Переменное количество безымянных аргументов (args)

и обычных документов (kwargs), позволяет передавать

случайное количество аргументов.
"""

def any_args(*args, **kwargs):
    nn=1
    for n in args:
        nn=nn*n

    for k in kwargs:
        print(k)
    return nn

print(any_args(5,7,8,10, name='Denis', age=19, birth='26-06-97'))