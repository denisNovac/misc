"""
func - аргумент, передаваемый функции i_want_function.

В такой аргумент можно передавать функции.
"""


def i_want_function(func, *args):
    print(func)
    return func(args[0], args[1])


def i_am_function(a, b):
    return a+b

print(i_want_function(i_am_function, 5, 8))


